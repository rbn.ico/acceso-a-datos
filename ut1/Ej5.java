/*
*
*
* 5. Realiza la aplicación que lea y muestre los datos del fichero XML creado por la anterior aplicación.
*
*
*
*/

import java.io.*;
import java.util.*;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import javax.xml.transform.*;
import javax.xml.transform.stream.*;
import javax.xml.transform.dom.*;
import org.xml.sax.*;

public class Ej5
{
	public static void main(String[] args) throws SAXException,IOException,ParserConfigurationException {

    	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document documento = builder.parse("Personas.xml");

        NodeList personas = documento.getElementsByTagName("persona");

        for (int i = 0; i < personas.getLength(); i++) {
            Node persona = personas.item( i ) ;
            Element elemento = ( Element ) persona ;
            System.out.print(elemento.getElementsByTagName("nombre").item(0).getTextContent()+": ");
            System.out.print(elemento.getElementsByTagName("edad").item(0).getTextContent());
            System.out.println();
        }
    }
}
