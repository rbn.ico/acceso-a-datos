/*
 *
 *
 * 4. Realiza la aplicación que genere un fichero XML con datos de personas como en el ejercicio 1
 *
 *
 *
 */
import javax.xml.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import java.util.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;

public class Ej4 {
	public static void main(String args[]) {
		ArrayList<Persona> personas = new ArrayList<Persona>();
		
		personas.add(new Persona());
		personas.add(new Persona());
		personas.add(new Persona());

		personas.get(0).setNombre("Jacinto"); personas.get(0).setEdad(34);
		personas.get(1).setNombre("Marina"); personas.get(1).setEdad(20);
		personas.get(2).setNombre("Eleuterio"); personas.get(2).setEdad(10);

		try{
			Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().getDOMImplementation().createDocument(null, "xml", null);

		Element rootElement = document.createElement("Personas");
		document.getDocumentElement().appendChild(rootElement);

		Element level1 = null, level2 = null;
		Text texto = null;

		for(Persona p : personas) {
			level1 = document.createElement("persona");
			rootElement.appendChild(level1);
			level2 = document.createElement("nombre");
			level1.appendChild(level2);
			texto = document.createTextNode(p.getNombre());
			level2.appendChild(texto);
			level2 = document.createElement("edad");
			level1.appendChild(level2);
			texto = document.createTextNode(String.valueOf(p.getEdad()));
			level2.appendChild(texto);
		}
			TransformerFactory.newInstance().newTransformer().transform(new DOMSource(document),new StreamResult("Personas.xml"));
		}catch(Exception e) {e.printStackTrace();}
	}
}

class Persona {
	private String nombre;
	private int edad;

	public Persona(){}

	public void setNombre(String nombre) {this.nombre = nombre;}
	public void setEdad(int edad) {this.edad = edad;}

	public String getNombre() {return nombre;}
	public int getEdad() {return edad;}
}

