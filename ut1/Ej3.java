/*
 *
 *
 * 3. Basándote en los apuntes y ejemplos de 
 * 	http://chuwiki.chuidiang.org/index.php?title=Leer_y_modificar_fichero_propiedades_en_java
 * crea un fichero de propiedades con el siguiente contenido:
 *
 * #Fichero de configuración de la aplicación X
 *
 * version=1.2.3
 * lanzamiento=11/08/2021
 * standalone=yes
 * port=5858
 *
 *
 */
import java.io.*;
import java.util.*;

public class Ej3 {
	public static void main(String args[]) {

		Properties properties = new Properties();

		properties.setProperty("lanzamiento", "11/08/2021");
		properties.setProperty("version", "1.2.3");
		properties.setProperty("standalone", "yes");
		properties.setProperty("port", "5858");
		
		String m = "Fichero de configuracion de la aplicacion AC\n";
		try {
			properties.store(new FileOutputStream("prop.conf"),m);
		}catch(IOException ioe) {ioe.printStackTrace();}
	}
}
