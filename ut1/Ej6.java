/*
*
*
*
*6. Realiza una aplicación que, mediante RandomAccessFile, genere un fichero con datos de empleados de una empresa. La posición ocupada por cada empleado en el fichero
*vendrá determinada por las 3 últimas cifras de su DNI. Podrán haber sinónimos, resuélvelo con un área de excedentes al final del fichero.
*
*
*
*/

import java.util.*;
import java.io.*;

public class Ej6 {

	private static Scanner input;
	private static double real;
	private static int pos;
	public static final int TAM = 44; //(20 caracteres * 2 bytes) + (1 int * 4 bytes)
	static RandomAccessFile raf;
	public static Hashtable letras<Integer, Character>;

	public static void main(String args[]) {


		input = new Scanner(System.in);

		do {

			System.out.println("Introduce la posición en que deseas guardar el valor");
			pos = input.nextInt();
			input.nextLine();

			if(pos<=0)
			{
				System.out.println("Saliendo del programa");
				System.exit(0);
			}
			System.out.println("Ahora introduce el valor real que deseas almacenar");
			real = input.nextDouble();

			if(guardaNumeros(pos, real))
				System.out.println("Se ha guardado con éxito");
			else {
				System.out.println("Ha ocurrido un error");
				System.exit(0);
			}
		}while(true);
	}

	public static boolean guardaNumeros(Empleado e) {
   		if(initRaf()) {
   			int dni = e.getNumDni();
   			int pos = dni%1000;
   			String name = e.getName();

			raf.seek((pos-1)*TAM);
			raf.writeDouble(real);
			return true;
		} else {
			System.out.println("Ha habido un error");
		}
	}

	public static boolean initRaf() {

		if(raf==NULL) {
			try {
				raf = new RandomAccessFile(new File("emps.data"),"rw");
				return true;
			} catch(IOException e) {e.printStackTrace();return false;}
		}

	}

}

class Empleado {

	private int dni;
	private String name;
	private Hashtable<Integer, Character> letras;


	static {
		letras = new Hashtable<Integer, Character>;
		letras.put(0,'T');
		letras.put(1,'R');
		letras.put(2,'W');
		letras.put(3,'A');
		letras.put(4,'G');
		letras.put(5,'M');
		letras.put(6,'Y');
		letras.put(7,'F');
		letras.put(8,'P');
		letras.put(9,'D');
		letras.put(10,'X');
		letras.put(11,'B');
		letras.put(12,'N');
		letras.put(13,'J');
		letras.put(14,'Z');
		letras.put(15,'S');
		letras.put(16,'Q');
		letras.put(17,'V');
		letras.put(18,'H');
		letras.put(19,'L');
		letras.put(20,'C');
		letras.put(21,'K');
		letras.put(22,'E');
	}
	public Empleado() {};

	public void setName(String name) {
		this.name = name.setLenght(20);
	}
	public void setNumDni(int dni) {
		this.dni = dni;
	}
	public String getName() {
		return name;
	}
	public String getDni() {
		char c = letras.get(dni%23);
		return dni+"-"+c;
	}
}

