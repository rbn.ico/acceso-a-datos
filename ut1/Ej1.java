/*
 *
 *
 * 1. Realiza una aplicación que muestre las propiedades del usuario en el sistema
 *
 *
 */

public class Ej1 {
	public static void main(String args[]) {

		System.getProperties().forEach( (key, value) -> System.out.println(key + ":\t" + value ));
	}
}
